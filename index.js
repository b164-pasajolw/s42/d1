


const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');

const spanFullName = document.querySelector('#span-full-name')



//Other Methods aside from above
//document.getElementById('txt-first-name')
//document.getElementsByClassName('txt-inputs')
//document.getElementsByTagName('input')


//for Interaction purposes
//keyup - a method use on event listener
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
})

//checking of inputs on the console
txtFirstName.addEventListener('keyup', (e) => {
	console.log(e.target)
	//e is for event
	console.log(e.target.value)
})


txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = `${txtFirstName.value}` + ` ${txtLastName.value}`
})

//checking of inputs on the console
txtLastName.addEventListener('keyup', (e) => {
	console.log(e.target)
	//e is for event
	console.log(e.target.value)
})




/*
//alternative from Ms. Judy:

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');

const spanFullName = document.querySelector('#span-full-name')

const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txt.LastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('keyup', updateFullName);

txtLastName.addEventListener('keyup', updateFullName)

*/
